# Projekt: Nginx, PHP, MySQL und Monitoring mit Grafana und Prometheus

## Inhaltsverzeichnis

1. Projektübersicht
2. Voraussetzungen
3. Verzeichnisstruktur
4. Einrichtung der Docker-Umgebung
5. Starten der Docker-Container
6. Zugriff auf die Webanwendung
7. Einrichtung von Grafana
8. Troubleshooting
9. Fazit

## 1. Projektübersicht

In diesem Projekt richte ich einen Nginx-Webserver mit PHP, MySQL und einem Monitoring-System mit Grafana und Prometheus ein. Das Ziel ist, dass ich mich als Schüler bei einer Webseite registrieren kann und das dann in einer Datenbank anschauen kann. Das ganze sollte auch mit einem Monitoringtool überwacht werden. 

## 2. Voraussetzungen

Ich brauche einen PC mit Strom. Bevor ich beginne, stelle ich sicher, dass Docker und Docker Compose auf meinem System installiert sind.

## 3. Verzeichnisstruktur

Ich erstelle die folgende Verzeichnisstruktur für das Projekt:

![Alt text](image-1.png)


## 4. Einrichtung der Docker-Umgebung

### Docker-Compose-Datei erstellen

Ich erstelle die Datei `docker-compose.yml` 
nginx: Webserver, läuft auf Port 80.
php: PHP-Dienst, teilt sich HTML-Dateien mit nginx.
mysql: MySQL-Datenbank (Version 5.7) mit initialem SQL-Skript.
mysql-exporter: Überwacht MySQL mit Prometheus.
prometheus: Speichert Metriken, läuft auf Port 9090.
grafana: Visualisiert Metriken, läuft auf Port 3000.

### PHP-Dockerfile erstellen

Ich erstelle die Datei `php/Dockerfile`
Das brauche ich um die mysqli erweiterung herunterzuladen.

### Prometheus-Konfigurationsdatei erstellen

Ich erstelle die Datei `prometheus/prometheus.yml`
mysql: Zielt auf mysql-exporter auf Port 9104.
nginx-exporter: Zielt auf nginx-exporter auf Port 9113.

### Nginx-Konfigurationsdatei erstellen

Ich erstelle die Datei `nginx/default.conf`
Sie behandelt Anfragen für PHP-Dateien mit fastcgi und leitet sie an einen PHP-Dienst auf Port 9000 weiter.

### PHP-Datei erstellen

Ich erstelle die Datei `php/index.php`
Nginx-Konfiguration:

Hört auf Port 80.
Root-Verzeichnis ist /var/www/html.
Indexdateien sind index.php, index.html, index.htm.
Handhabt PHP-Dateien mit fastcgi und leitet sie an php:9000.
PHP-Datei:

Verbindet sich mit einer MySQL-Datenbank.
Fügt Schülerdaten (vorname, name, schule, klasse) in die students-Tabelle ein, wenn ein Formular abgeschickt wird.
Gibt eine Erfolgsmeldung oder einen Fehler aus.
HTML-Formular zur Eingabe der Daten.

### MySQL-Initialisierungsdatei erstellen

Ich erstelle die Datei `mysql/init.sql`
Ich habe eine Datenbank und eine Tabelle erstellt:

Datenbank: school, wird erstellt.
Tabelle: students, wird erstellt, mit folgenden Spalten:
id: Auto-Inkrement Primärschlüssel
vorname: VARCHAR(50)
name: VARCHAR(50)
schule: VARCHAR(50)
klasse: VARCHAR(10)

## 5. Starten der Docker-Container

Ich baue die Docker-Images neu und starte die Container:

1. Ich baue die Docker-Images neu mit `docker-compose build`.
2. Ich starte die Container neu mit `docker-compose up -d`.

## 6. Zugriff auf die Webanwendung

Ich öffne meinen Webbrowser und gehe zu `http://localhost`, um auf die Webanwendung zuzugreifen.

## 7. Einrichtung von Grafana

### Hinzufügen der Prometheus-Datenquelle

1. Ich öffne meinen Webbrowser und gehe zu `http://localhost:3000`.
2. Ich melde mich bei Grafana an und klicke auf **Configuration** > **Data Sources**.
3. Ich füge eine neue Prometheus-Datenquelle hinzu und konfiguriere sie mit der URL `http://prometheus:9090`.
Wenn die exporter Files richtig sind, dann sollte es funktionieren.

### Erstellen eines Dashboards

1. Ich klicke auf **Create** > **Dashboard**.
2. Ich füge ein neues Panel hinzu und konfiguriere es mit den gewünschten Prometheus-Abfragen bei mir ist die Abfrage ob der Service Up oder Down ist.

## 8. Troubleshooting

### Zugriff auf die Webanwendung

Wenn ich die Webseite nicht erreichen kann, überprüfe ich die Container-Logs und die Nginx-Konfiguration.
Wenn das Monitoring nicht funktioniert, dann überprüfe ich meine exporterfiles und meine jobs.

### Fehler in den Containern beheben

- **Nginx-Fehler**: Ich überprüfe die `nginx/default.conf` auf Syntaxfehler.
- **PHP-Fehler**: Ich stelle sicher, dass die `mysqli`-Erweiterung im Dockerfile in `php` installiert ist.
- **MySQL-Verbindung**: Ich überprüfe die MySQL-Logs auf Probleme mit der Datenbankverbindung.

Ich hatte zuerst einen Syntaxfehler und dann musste ich noch meine mysqli Erweiterung installieren.

## 9. Fazit

Mit diesem Setup habe ich erfolgreich einen Nginx-Webserver mit PHP, MySQL und einem Monitoring-System mit Grafana und Prometheus eingerichtet. Die Webanwendung läuft stabil und ich kann die Leistung mit Grafana und Prometheus überwachen. Das ganze Projekt war nicht so einfach, ich musste mindestens zwei Mal von neu anfangen, weil nichts funktioniert hat. Den Loadbalancer habe ich mir eigentlich auch vorgenommen aber habe es leider nicht mehr geschafft.

![Alt text](image.png)
