CREATE DATABASE IF NOT EXISTS school;
USE school;

CREATE TABLE IF NOT EXISTS students (
    id INT AUTO_INCREMENT PRIMARY KEY,
    vorname VARCHAR(50),
    name VARCHAR(50),
    schule VARCHAR(50),
    klasse VARCHAR(10)
);
