<?php
$servername = "mysql";
$username = "root";
$password = "example";
$dbname = "school";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $vorname = $_POST['vorname'];
    $name = $_POST['name'];
    $schule = $_POST['schule'];
    $klasse = $_POST['klasse'];

    $sql = "INSERT INTO students (vorname, name, schule, klasse) VALUES ('$vorname', '$name', '$schule', '$klasse')";

    if ($conn->query($sql) === TRUE) {
        echo "New record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

$conn->close();
?>

<!DOCTYPE html>
<html>
<body>

<h2>Schülerdaten eingeben</h2>
<form method="post">
  Vorname:<br>
  <input type="text" name="vorname" required>
  <br>
  Name:<br>
  <input type="text" name="name" required>
  <br>
  Schule:<br>
  <input type="text" name="schule" required>
  <br>
  Klasse:<br>
  <input type="text" name="klasse" required>
  <br><br>
  <input type="submit" value="Submit">
</form>

</body>
</html>
