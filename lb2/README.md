Dokumentation: Aufbau eines AWS-Clusters
In dieser Dokumentation erkläre ich, wie ich einen AWS-Cluster eingerichtet habe und die verschiedenen Services konfiguriert habe, um eine vollständige Webanwendung bereitzustellen. Ich werde die Schritte und Überlegungen detailliert beschreiben, die ich während dieses Prozesses durchlaufen habe.

1. AWS-Cluster erstellen

![cluster](image.png)

Ich habe einen AWS-Cluster erstellt, um eine Grundlage für meine Anwendung zu schaffen. Dabei habe ich mich für die ECS (Elastic Container Service) entschieden, um Container-Services zu verwalten.

Was ich gemacht habe:
ECS-Dashboard geöffnet: Ich bin zur AWS Management Console gegangen und habe nach „ECS“ gesucht.
Cluster erstellt: Ich habe auf „Cluster erstellen“ geklickt und den Typ „EC2 Linux + Networking“ ausgewählt.
Cluster konfiguriert: Ich habe meinem Cluster einen Namen gegeben und die Standard-Einstellungen übernommen.
Cluster gestartet: Nach der Konfiguration habe ich auf „Cluster erstellen“ geklickt, um meinen Cluster einzurichten.
Wieso ich das gemacht habe:
Der Cluster bildet die Basis für das Hosting meiner Container und ermöglicht es mir, Anwendungen skalierbar und zuverlässig zu betreiben.

2. Webserver-Service einrichten

![service](image-1.png)
Nachdem mein Cluster eingerichtet war, habe ich den Webserver-Service konfiguriert, um meine Webanwendung bereitzustellen.

Was ich gemacht habe:
Service erstellt: Ich bin zu meinem Cluster gegangen und habe auf „Service erstellen“ geklickt.
Aufgaben-Definition ausgewählt: Ich habe eine bestehende Aufgaben-Definition ausgewählt, die einen NGINX-Webserver definiert.
Service konfiguriert: Ich habe die Launch-Type-Option „EC2“ gewählt und die Anzahl der Aufgaben (Webserver-Instanzen) festgelegt.
Service gestartet: Ich habe den Service erstellt, um den Webserver zu starten und auf Anfragen zu warten.
Wieso ich das gemacht habe:
Der Webserver-Service stellt sicher, dass meine Webanwendung für Benutzer erreichbar ist und Anfragen verarbeitet werden können.

3. RDS-Datenbank einrichten

![db1](image-2.png)
![db2](image-3.png)
Um eine relational Datenbank für meine Webanwendung bereitzustellen, habe ich Amazon RDS verwendet.

Was ich gemacht habe:
RDS-Dashboard geöffnet: Ich habe nach „RDS“ in der AWS Management Console gesucht.
Datenbank erstellt: Ich habe auf „Datenbank erstellen“ geklickt und MySQL als Datenbank-Engine ausgewählt.
Datenbank konfiguriert: Ich habe die Instanzklasse, Speichergröße und Benutzerdaten für die Datenbank konfiguriert.
Datenbank gestartet: Nach der Konfiguration habe ich die Datenbank erstellt und gestartet.
Wieso ich das gemacht habe:
Amazon RDS erleichtert die Verwaltung von relationalen Datenbanken, einschließlich Backups, Updates und Skalierung, was mir hilft, mich auf die Anwendung statt auf die Datenbankverwaltung zu konzentrieren.

4. Load Balancer einrichten

![LB](image-4.png)
Um den Datenverkehr auf mehrere Webserver-Instanzen zu verteilen, habe ich einen Load Balancer konfiguriert.

Was ich gemacht habe:
EC2-Dashboard geöffnet: Ich habe nach „EC2“ in der AWS Management Console gesucht.
Load Balancer erstellt: Ich habe „Load Balancer“ ausgewählt und auf „Create Load Balancer“ geklickt.
Load Balancer konfiguriert: Ich habe einen „Instace Load Balancer“ ausgewählt und die notwendigen Netzwerkeinstellungen (VPC, Subnetze) konfiguriert.
Listener und Zielgruppen erstellt: Ich habe einen HTTP-Listener eingerichtet und eine Zielgruppe erstellt, die die EC2-Instanzen des Webservers umfasst.
Wieso ich das gemacht habe:
Der Load Balancer sorgt dafür, dass der Datenverkehr gleichmäßig auf die Webserver verteilt wird und erhöht die Verfügbarkeit und Zuverlässigkeit meiner Anwendung.

5. Sicherheitsgruppe konfigurieren

![Sicherheitsgruppen](image-5.png)
Ich habe eine Sicherheitsgruppe erstellt, um den Zugriff auf meine EC2-Instanzen und die RDS-Datenbank zu steuern.

Was ich gemacht habe:
Sicherheitsgruppen-Dashboard geöffnet: Ich habe „Sicherheitsgruppen“ unter „Netzwerk & Sicherheit“ in der EC2-Konsole ausgewählt.
Sicherheitsgruppe erstellt: Ich habe eine neue Sicherheitsgruppe erstellt und Regeln für eingehenden und ausgehenden Verkehr definiert.
Regeln konfiguriert: Ich habe Regeln erstellt, die HTTP- und HTTPS-Verkehr zum Webserver und den Datenbankzugriff entsprechend meiner Anforderungen ermöglichen.
Wieso ich das gemacht habe:
Sicherheitsgruppen agieren als virtuelle Firewall und schützen meine Instanzen und Datenbank vor unbefugtem Zugriff und Angriffen.

6. Image-Registry einrichten
![Registry](image-6.png)
Für die Verwaltung meiner Docker-Container-Images habe ich Amazon ECR (Elastic Container Registry) verwendet.

Was ich gemacht habe:
ECR-Dashboard geöffnet: Ich habe nach „ECR“ in der AWS Management Console gesucht.
Repository erstellt: Ich habe ein neues Repository erstellt, in dem meine Docker-Images gespeichert werden.
Images hochgeladen: Ich habe die Docker-Images in das Repository hochgeladen, um sie für meine ECS-Aufgaben verfügbar zu machen.
